package net.idear.justjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.TextView;

public class LoadingActivity extends AppCompatActivity {

    String nombre_maquina = "";
    TextView tv;
    int i = 0;
    String textos[] = {"Verificando Desbalanceo", "Verificando Desalineación", "Verificando Solturas", "Verificando Rodamientos", "Verificando Engranajes", "Verificando Lubricación"};
    CountDownTimer cdt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nombre_maquina = getIntent().getStringExtra("MACHINE_NAME");
        setTitle(nombre_maquina);
        setContentView(R.layout.activity_loading);

        tv = findViewById(R.id.tv_proceso);
        tv.setText(textos[0]);
        cdt = new CountDownTimer(9000, 1500) {
            public void onTick(long millisUntilFinished) {
                tv.setText(textos[i]);
                i++;
                if(i > 5) i = 5;
            }

            public void onFinish() {
                Intent intent = new Intent(getBaseContext(), DetectoresActivity.class);
                intent.putExtra("MACHINE_NAME", nombre_maquina);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivityForResult(intent, 0);
            }
        }.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        this.finish();
    }

    @Override
    public void onBackPressed() {
        cdt.cancel();
        super.onBackPressed();
    }
}
