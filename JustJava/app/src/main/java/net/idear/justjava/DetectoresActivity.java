package net.idear.justjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class DetectoresActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getIntent().getStringExtra("MACHINE_NAME") + " - FALLAS");
        setContentView(R.layout.activity_detectores);
    }
}
