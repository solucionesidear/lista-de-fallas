package net.idear.justjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class PuntoActivity extends AppCompatActivity {

    String nombre_maquina = "";
    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nombre_maquina = getIntent().getStringExtra("MACHINE_NAME");
        setTitle(nombre_maquina);
        setContentView(R.layout.activity_punto);

        //if (nombre_maquina.equals("REDUCTOR#2")) {
        //    iv = findViewById(R.id.imageView);
        //    iv.setImageResource(R.drawable.punto2);
        //}
    }

    public void Medir_OnClick(View v) {
        Intent intent = new Intent(getBaseContext(), LoadingActivity.class);
        intent.putExtra("MACHINE_NAME", nombre_maquina);
        startActivity(intent);
    }
}
