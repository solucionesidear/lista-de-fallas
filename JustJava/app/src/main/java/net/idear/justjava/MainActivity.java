package net.idear.justjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("BOMBAS DE ALTA");
        setContentView(R.layout.activity_main);
        ImageView iv = findViewById(R.id.reductor2_image_view);
        iv.setClipToOutline(true);
    }

    public void MaquinaItem_OnClick(View v){
        String name = v.getTag().toString();
        Intent intent = new Intent(getBaseContext(), PuntoActivity.class);
        intent.putExtra("MACHINE_NAME", name);
        startActivity(intent);
    }
}


